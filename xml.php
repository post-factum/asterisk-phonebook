<?php
	require "check_access.php";
	if (!check_access())
		die();

	require "connect_db.php";
	$link = connect_db();
	$query = "SELECT `id`, `num`, `name` FROM `phonebook` ORDER BY `name`;";
	$res = mysqli_query($link, $query);
	header('Content-Type: application/xml; charset=utf-8');
	echo '<?xml version="1.0" encoding="utf-8"?>'."\n";
	echo '<YealinkIPPhoneBook>'."\n";
	echo '<Title>Yealink</Title>'."\n";
	echo '<Menu Name="Asterisk">'."\n";
	while ($rows = mysqli_fetch_assoc($res))
	{
		echo '<Unit Name="'.$rows["name"].'" default_photo="" Phone3="" Phone2="" Phone1="'.$rows["num"].'" />'."\n";
	}
	echo '</Menu>'."\n";
	echo '</YealinkIPPhoneBook>'."\n";
	mysqli_close($link);
?>
