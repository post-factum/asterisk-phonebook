<?php
    function connect_db()
    {
        require "config.php";
        $link = mysqli_connect($config["mysql_host"].":".$config["mysql_port"], $config["mysql_user"], $config["mysql_passwd"]);
        if (!$link)
            return null;
        $res = mysqli_select_db($link, $config["mysql_db"]);
        if (!$res)
        {
            mysqli_close($link);
            return null;
        }
        return $link;
    }
?>
